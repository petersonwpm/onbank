# onbank

Simples projeto para estudo Web.

docker-compose up -d

- Baixar dependências
docker-compose exec -u $(id -u):$(id -g) appserver composer install

- Cria estrutura de tabela
docker-compose exec -u $(id -u):$(id -g) appserver php vendor/bin/phinx migrate

- Cria dados na base
docker-compose exec -u $(id -u):$(id -g) appserver php vendor/bin/phinx seed:run

- Acessar via psql
docker-compose exec database psql -U onbank

- Descobrir ip do banco
docker-compose exec database ip addr

- Acessar
http://localhost:8080
