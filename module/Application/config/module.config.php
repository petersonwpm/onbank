<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'client' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/client[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ClientController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
             'agency' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/agency[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AgencyController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
             'account' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/account[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AccountController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
             'account-movement' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/movement[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AccountMovementController::class,
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Model\ClientTable::class => function ($container) {
                $gateway = $container->get(Gateway\ClientsTableGateway::class);
                return new Model\ClientTable($gateway);
            },
            Model\AgencyTable::class => function ($container) {
                return new Model\AgencyTable(
                    new TableGateway('agencias', $container->get(AdapterInterface::class))
                );
            },
            Model\AccountTypeTable::class => function ($container) {
                return new Model\AccountTypeTable(
                    new TableGateway('tipos_de_conta', $container->get(AdapterInterface::class))
                );
            },
            Model\AccountTable::class => function ($container) {
                return new Model\AccountTable(
                    new TableGateway('contas', $container->get(AdapterInterface::class)),
                    new TableGateway('agencias', $container->get(AdapterInterface::class)),
                    new TableGateway('clientes', $container->get(AdapterInterface::class)),
                    new TableGateway('tipos_de_conta', $container->get(AdapterInterface::class))
                );
            },
            Model\AccountMovementTypeTable::class =>  function ($container) {
                return new Model\AccountMovementTypeTable(
                    new TableGateway('tipos_de_movimento', $container->get(AdapterInterface::class))
                );
            },
            Model\AccountMovementTable::class => function ($container) {
                return new Model\AccountMovementTable(
                    new TableGateway('movimentos', $container->get(AdapterInterface::class)),
                    $container->get(Model\AccountTable::class),
                    $container->get(Model\AccountMovementTypeTable::class)
                );
            },
            Gateway\ClientsTableGateway::class => function ($container) {
                $dbAdapter = $container->get(AdapterInterface::class);
                return new TableGateway('clientes', $dbAdapter);
            }
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\AccountMovementController::class => function ($container) {
                return new Controller\AccountMovementController(
                    $container->get(Model\AccountTable::class),
                    $container->get(Model\AccountMovementTypeTable::class),
                    $container->get(Model\AccountMovementTable::class)
                );
            },
            Controller\ClientController::class => function ($container) {
                $model = $container->get(Model\ClientTable::class);
                return new Controller\ClientController($model);
            },
            Controller\AgencyController::class => function ($container) {
                $model = $container->get(Model\AgencyTable::class);
                return new Controller\AgencyController($model);
            },
            Controller\AccountController::class => function ($container) {
                $model = $container->get(Model\AccountTable::class);
                $client = $container->get(Model\ClientTable::class);
                $agency = $container->get(Model\AgencyTable::class);
                $accountType = $container->get(Model\AccountTypeTable::class);
                return new Controller\AccountController($model, $client, $agency, $accountType);
            },
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
