<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class ClientForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('client');

        $this->add([
            'name' => 'codcli',
            'type' => 'text',
            'options' => [
                'label' => 'Código',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'C1',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Nome',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'José da Silva',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'cpf',
            'type' => Element\Number::class,
            'options' => [
                'label' => 'CPF',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '123456789011',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'type-person',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Pessoa',
                'value_options' => [
                    [
                        'value' => 'F',
                        'label' => 'Física',
                    ],
                    [
                        'value' => 'J',
                        'label' => 'Jurídica',
                    ]
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'city',
            'type' => 'text',
            'options' => [
                'label' => 'Cidade',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Porto Alegre',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'status',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Situação',
                'value_options' => [
                    '0' => 'Inativo',
                    '1' => 'Ativo',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],

        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
