<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class AccountForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('account');

        $this->add([
            'name' => 'code',
            'type' => 'text',
            'options' => [
                'label' => 'Código',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => '0001',
                'required' => true,
            ]
        ]);
        $this->add([
            'name' => 'codag',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Agência',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'codcli',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Cliente',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'limit',
            'type' => 'text',
            'options' => [
                'label' => 'Limite'
            ],
            'attributes' => [
                'class' => 'form-control',
            ]
        ]);
        $this->add([
            'name' => 'account-type',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Tipo de Conta',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
