<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class AccountMovementForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('account-movement');

        $this->add([
            'name' => 'account',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Conta',
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly' => true,
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => 'movement-type',
            'type' => Element\Select::class,
            'options' => [
                'label' => 'Tipo de Movimento',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => 'value',
            'type' => Element\Text::class,
            'options' => [
                'label' => 'Valor',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ]
        ]);

        $this->add([
            'name' => 'date',
            'type' => Element\Date::class,
            'options' => [
                'label' => 'Data Movimento',
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
