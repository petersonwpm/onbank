<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class AgencyForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('agency');

        $this->add([
            'name' => 'codag',
            'type' => 'text',
            'options' => [
                'label' => 'Código',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'A1',
                'required' => true,
            ]
        ]);
        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Nome',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Agência Ex Torquindo',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'city',
            'type' => 'text',
            'options' => [
                'label' => 'Cidade',
            ],
            'attributes' => [
                'class' => 'form-control',
                'placeholder' => 'Porto Alegre',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
