<?php
namespace Application\Controller;

use Application\Entity\AccountMovement;
use Application\Form\AccountMovementForm;
use Application\Model\AccountMovementTable;
use Application\Model\AccountMovementTypeTable;
use Application\Model\AccountTable;
use DateTime;
use Exception;
use Zend\Form\Annotation\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AccountMovementController extends AbstractActionController
{
    /**
     * @var \Application\Model\AccountTable $account
     */
    private $account;

    /**
     * @var \Application\Model\AccountMovementTypeTable  $movementType
     */
    private $movementType;

    /**
     * @var \Application\Model\AccountMovementTable $movement
     */
    private $movement;

    public function __construct(AccountTable $account, AccountMovementTypeTable $movementType, AccountMovementTable $movement)
    {
        $this->account      = $account;
        $this->movementType = $movementType;
        $this->movement     = $movement;
    }

    public function moveAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('account', ['action' => 'index']);
        }

        $account = $this->account->findByCode($id);
        $listMovementTypes = [];
        foreach ($this->movementType->findAll() as $movementType) {
            $listMovementTypes[$movementType->getCode()] = $movementType->getDescription();
        }

        $form = new AccountMovementForm();
        $form->get('account')->setValue($account->getCode());
        $form->get('movement-type')->setValueOptions($listMovementTypes);

        $form->get('submit')->setValue('add');

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return ['id' => $id, 'form' => $form];
        }

        // filtro vazio, mas o correto é add validadores
        //$form->setInputFilter();

        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $data = $form->getData();

        $accountMovement =  $this->movementType->findByCode($data['movement-type']);

        try {
            $this->movement->save(new AccountMovement(
                $account,
                $accountMovement,
                new DateTime($data['date']),
                $data['value']
            ));
        } catch (Exception $e) {
            $this->flashMessenger()->addMessage('Não consegui salvar... acho que duplicou registro, mas vá saber. Tente outra data!');

            return ['id' => $id, 'form' => $form, 'messages' => $this->flashMessenger()->getMessages()];
        }

        $this->flashMessenger()->addMessage('Transação realizada com sucesso... eu espero');
        $this->redirect()->toRoute('account', ['action' => 'index']);
    }

    public function listAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            $movements = $this->movement->findAll();

            return new ViewModel([
                'movements' => $movements,
            ]);
        }

        $movements = $this->movement->findByAccountId($id);

        return new ViewModel([
            'movements' => $movements,
        ]);
    }
}
