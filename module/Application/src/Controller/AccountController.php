<?php
namespace Application\Controller;

use Application\Entity\Account;
use Application\Form\AccountForm;
use Application\Model\AccountTable;
use Application\Model\AccountTypeTable;
use Application\Model\AgencyTable;
use Application\Model\ClientTable;
use DateTime;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Exception;

class AccountController extends AbstractActionController
{
    /**
     * @var \Application\Model\AccountTable
     */
    private $repository;

    /**
     * @var \Application\Model\ClientTable
     */
    private $client;

    public function __construct(
        AccountTable $table,
        ClientTable $client,
        AgencyTable $agency,
        AccountTypeTable $accountType
    )
    {
        $this->repository = $table;
        $this->client     = $client;
        $this->agency     = $agency;
        $this->accountType = $accountType;
    }

    public function indexAction()
    {
        $accounts = $this->repository->findAll();
        return new ViewModel([
            'messages' => $this->flashMessenger()->getMessages(),
            'accounts' => $accounts
        ]);
    }

    public function addAction()
    {
        try {
            $form = new AccountForm();
            $form->get('submit')->setValue('add');

            $listClients = [];
            foreach ($this->client->findAll() as $client) {
                $listClients[$client->getCode()] = $client->getName();
            }
            $form->get('codcli')->setValueOptions($listClients);

            $listAgencies = [];
            foreach ($this->agency->findAll() as $agency) {
                $listAgencies[$agency->getCode()] = $agency->getName();
            }
            $form->get('codag')->setValueOptions($listAgencies);

            $listTypes = [];
            foreach ($this->accountType->findAll() as $accountType) {
                $listTypes[$accountType->getCode()] = $accountType->getDescription();
            }
            $form->get('account-type')->setValueOptions($listTypes);

            $request = $this->getRequest();
            if (! $request->isPost()) {
                return ['form' => $form];
            }

            // filtro vazio, mas o correto é add validadores
            $form->setInputFilter(new InputFilter());

            $form->setData($request->getPost());

            if (! $form->isValid()) {
                return ['form' => $form];
            }

            $data = $form->getData();

            $client      = $this->client->findByCode($data['codcli']);
            $agency      = $this->agency->findByCode($data['codag']);
            $accountType = $this->accountType->findByCode($data['account-type']);

            $account = new Account(
                $data['code'],
                $data['limit']
            );
            $account->setClient($client)
                    ->setAgency($agency)
                    ->setAccountType($accountType);


            $this->repository->save($account);

            return $this->redirect()->toRoute('account');
        } catch (Exception $exception) {
            $this->flashMessenger()->addMessage($exception->getMessage());
            return new ViewModel([
                'messages' => $this->flashMessenger()->getMessages(),
                'form' => $form
            ]);
        }
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('account', ['action' => 'add']);
        }

        try {
            $account = $this->repository->findByCode($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('account', ['action' => 'index']);
        }

        $form = new AccountForm();
        $listClients = [];
        foreach ($this->client->findAll() as $client) {
            $listClients[$client->getCode()] = $client->getName();
        }
        $form->get('codcli')->setValueOptions($listClients);
        $form->get('codcli')->setValue($account->getClient()->getCode());

        $listAgencies = [];
        foreach ($this->agency->findAll() as $agency) {
            $listAgencies[$agency->getCode()] = $agency->getName();
        }
        $form->get('codag')->setValueOptions($listAgencies);
        $form->get('codag')->setValue($account->getAgency()->getCode());

        $listTypes = [];
        foreach ($this->accountType->findAll() as $accountType) {
            $listTypes[$accountType->getCode()] = $accountType->getDescription();
        }
        $form->get('account-type')->setValueOptions($listTypes);
        $form->get('account-type')->setValue($account->getAccountType()->getCode());

        $form->get('limit')->setValue($account->getLimit());
        $form->get('code')->setValue($account->getCode());

        $form->bind($account);
        $form->get('submit')->setAttribute('value', 'Editar');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $data = $request->getPost();

        $client      = $this->client->findByCode($data['codcli']);
        $agency      = $this->agency->findByCode($data['codag']);
        $accountType = $this->accountType->findByCode($data['account-type']);

        $account = new Account(
            $data['code'],
            $data['limit']
        );
        $account->setClient($client)
                ->setAgency($agency)
                ->setAccountType($accountType);

        $this->repository->update($account);

        // Redirect to album list
        return $this->redirect()->toRoute('account', ['action' => 'index']);
    }

    public function deleteAction()
    {
        try {
            $id = $this->params()->fromRoute('id', '');

            if (empty($id)) {
                return $this->redirect()->toRoute('account', ['action' => 'add']);
            }

            $this->repository->delete($id);
        } catch (Exception $exception) {
            $this->flashMessenger()->addMessage('Não foi possível remover a conta ' . $id);
        }

        return $this->redirect()->toRoute('account', ['action' => 'index']);
    }
}
