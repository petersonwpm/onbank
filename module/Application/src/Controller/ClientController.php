<?php
namespace Application\Controller;

use Application\Entity\Client;
use Application\Form\ClientForm;
use Application\Model\ClientTable;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ClientController extends AbstractActionController
{
    /**
     * @var ClientsGateway $repository
     */
    private $repository;

    public function __construct(ClientTable $clients)
    {
        $this->repository = $clients;
    }

    public function indexAction()
    {
        $clients = $this->repository->findAll();

        return new ViewModel([
            'messages' => $this->flashMessenger()->getMessages(),
            'clients' => $clients
        ]);
    }

    public function addAction()
    {
        $form = new ClientForm();
        $form->get('submit')->setValue('add');

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $form->setInputFilter(new InputFilter());

        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $data = $form->getData();

        $client = new Client(
            $data['codcli'],
            $data['name'],
            $data['cpf'],
            $data['type-person'],
            $data['city'],
            $data['status']
        );

        $this->repository->save($client);

        return $this->redirect()->toRoute('client');
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('client', ['action' => 'add']);
        }

        try {
            $client = $this->repository->findByCode($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('client', ['action' => 'index']);
        }

        $form = new ClientForm();
        $form->get('type-person')
             ->setValue($client->getTypePerson());
        $form->get('status')->setValue($client->getStatus());

        $form->bind($client);
        $form->get('submit')->setAttribute('value', 'Editar');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $data = $request->getPost();

        $client = new Client(
            $data['codcli'],
            $data['name'],
            $data['cpf'],
            $data['type-person'],
            $data['city'],
            $data['status']
        );
        $this->repository->update($client);

        return $this->redirect()->toRoute('client', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('client', ['action' => 'add']);
        }

        $this->repository->delete($id);

        $this->flashMessenger()->addMessage('Cliente ' . $id . ' removido com sucesso');

        return $this->redirect()->toRoute('client', ['action' => 'index']);
    }
}
