<?php
namespace Application\Controller;

use Application\Entity\Agency;
use Application\Form\AgencyForm;
use Application\Model\AgencyTable;
use Zend\InputFilter\InputFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Exception;

class AgencyController extends AbstractActionController
{
    /**
     * @var \Application\Model\AgencyTable
     */
    private $repository;

    public function __construct(AgencyTable $table)
    {
        $this->repository = $table;
    }

    public function indexAction()
    {
        $agencies = $this->repository->findAll();
        $messages = $this->flashMessenger()->getMessages();
        return new ViewModel([
            'messages' => $messages,
            'agencies' => $agencies
        ]);
    }

    public function addAction()
    {
        $form = new AgencyForm();
        $form->get('submit')->setValue('add');

        $request = $this->getRequest();
        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $form->setInputFilter(new InputFilter());

        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $data = $form->getData();

        $agency = new Agency(
            $data['codag'],
            $data['name'],
            $data['city']
        );

        $this->repository->save($agency);

        return $this->redirect()->toRoute('agency');
        return new ViewModel();
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('agency', ['action' => 'add']);
        }

        try {
            $agency = $this->repository->findByCode($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('agency', ['action' => 'index']);
        }

        $form = new agencyForm();
        $form->bind($agency);
        $form->get('submit')->setAttribute('value', 'Editar');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $data = $request->getPost();

        $agency = new agency(
            $data['codag'],
            $data['name'],
            $data['city']
        );
        $this->repository->update($agency);

        return $this->redirect()->toRoute('agency', ['action' => 'index']);
    }

    public function deleteAction()
    {
        try {
        $id = $this->params()->fromRoute('id', '');

        if (empty($id)) {
            return $this->redirect()->toRoute('agency', ['action' => 'add']);
        }

        $this->repository->delete($id);

        } catch (Exception $exception) {
            $this->FlashMessenger()->addMessage('Não foi possivel deletar a agência: ' . $id . '. Verifique se não existem contas vinculadas');
        }


        return $this->redirect()->toRoute('agency', ['action' => 'index']);
    }

    public function listAccounts()
    {
        //$id = $this->params()->fromRoute('id', '');
        return new ViewModel();
    }
}
