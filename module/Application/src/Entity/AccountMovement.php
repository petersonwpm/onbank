<?php
namespace Application\Entity;

use Application\Entity\Account;
use Application\Entity\AccountMovementType;
use DateTime;

class AccountMovement
{
    /**
     * @var Application\Entity\Account $accout
     */
    private $account;

    /**
     * @var Application\Entity\AccountMovementType
     */
    private $accountMovementType;

    /**
     * @var DateTime $date
     */
    private $date;

    /**
     * @var float $value
     */
    private $value;

    public function __construct(Account $account, AccountMovementType $accountMovementType, DateTime $date, float $value)
    {
        $this->account             = $account;
        $this->accountMovementType = $accountMovementType;
        $this->date                = $date;
        $this->value               = $value;
    }

    public function getAccount() : Account
    {
        return $this->account;
    }

    public function getType() : AccountMovementType
    {
        return $this->accountMovementType;
    }

    public function getDate() : DateTime
    {
        return $this->date;
    }

    public function getValue()
    {
        return $this->value;
    }
}
