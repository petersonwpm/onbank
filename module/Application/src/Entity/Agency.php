<?php
namespace Application\Entity;

class Agency
{
    /**
     * @var string $code;
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $city
     */
    private $city;

    public function __construct(string $codag, string $name, string $city)
    {
        $this->code = $codag;
        $this->name = $name;
        $this->city = $city;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getArrayCopy()
    {
        return [
            'codag' => $this->code,
            'name'  => $this->name,
            'city'  => $this->city,
        ];
    }
}
