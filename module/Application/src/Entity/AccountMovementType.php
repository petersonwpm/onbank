<?php
namespace Application\Entity;

class AccountMovementType
{
    private $code;
    private $description;
    private $debitCredit;

    public function __construct(string $code, string $description, string $debitCredit)
    {
        $this->code = $code;
        $this->description = $description;
        $this->debitCredit = $debitCredit;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function getDebitCredit() : string
    {
        return $this->debitCredit;
    }
}
