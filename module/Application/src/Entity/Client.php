<?php
namespace Application\Entity;

class Client
{
    const PHISICAL_PERSON = 'F';

    const JURIDIC_PERSON  = 'J';
    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $cpf
     */
    private $cpf;

    /**
     * Pessoa [Fisica, Juridica]
     * @var string typePerson
     */
    private $typePerson;

    /**
     * @var string $city
     */
    private $city;

    /**
     * @var bool $status
     */
    private $status;

    public function __construct(
        string $code,
        string $name,
        string $cpf,
        string $typePerson,
        string $city,
        bool   $status
    )
    {
        $this->code       = strtoupper(trim($code));
        $this->name       = trim($name);
        $this->cpf        = trim($cpf);

        if ($typePerson != self::PHISICAL_PERSON && $typePerson != self::JURIDIC_PERSON) {
            throw new InvalidArgumentException('Somente F e J são válidos para Pessoa Júridica e Física');
        }
        $this->typePerson = $typePerson;
        $this->city       = ucwords(trim($city));
        $this->status     = $status;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getCpf() : string
    {
        return $this->cpf;
    }

    public function isPhisicalPerson() : bool
    {
        return $this->typePerson === Client::PHISICAL_PERSON;
    }

    public function isJuridicPerson() : bool
    {
        return $this->typePerson === Client::JURIDIC_PERSON;
    }

    public function getCity() : string
    {
        return $this->city;
    }

    public function getTypePerson()
    {
        return $this->typePerson;
    }

    public function getStatus() : bool
    {
        return $this->status;
    }

    public function getArrayCopy() : array
    {
        return [
            'codcli' => $this->code,
            'name' => $this->name,
            'cpf' => $this->cpf,
            'typePerson' => $this->typePerson,
            'city' => $this->city,
        ];
    }
}
