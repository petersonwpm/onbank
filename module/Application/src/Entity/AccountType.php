<?php
namespace Application\Entity;

class AccountType
{
    private $code;

    private $description;

    private $class;


    public function __construct(string $code, string $description, string $class)
    {
        $this->code = $code;
        $this->description = $description;
        $this->class = $class;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getClass()
    {
        return $class;
    }
}
