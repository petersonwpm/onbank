<?php
namespace Application\Entity;

use Application\Entity\AccountType;
use Application\Entity\Agency;
use DateTime;

class Account
{
    /**
     * @var string $code
     */
    private $code;

    /**
     * @var Agency $agency
     */
    private $agency;

    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var float $limit
     */
    private $limit;

    /**
     * @var DateTime $date
     */
    private $openDate;

    /**
     * @var string $type
     */
    private $type;

    public function __construct(string $code, float $limit)
    {
        $this->code = $code;
        $this->limit = $limit;
        $this->openDate = new DateTime('now');
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function setClient(Client $client) : self
    {
        $this->client = $client;
        return $this;
    }

    public function getClient() : client
    {
        return $this->client;
    }

    public function setAgency(Agency $agency) : self
    {
        $this->agency = $agency;
        return $this;
    }

    public function getAgency() : Agency
    {
        return $this->agency;
    }

    public function getLimit() : float
    {
        return $this->limit;
    }

    public function getOpenDate() : DateTime
    {
        return $this->openDate;
    }

    public function setAccountType(AccountType $accountType) : self
    {
        $this->type = $accountType;
        return $this;
    }

    public function getAccountType() : AccountType
    {
        return $this->type;
    }

    public function getArrayCopy() : array
    {
        return [];
    }
}
