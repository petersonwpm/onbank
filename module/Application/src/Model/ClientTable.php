<?php
namespace Application\Model;

use Application\Entity\Client;
use Zend\Db\TableGateway\TableGatewayInterface;

class ClientTable
{
    private $gateway;

    public function __construct(TableGatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    public function findByCode(string $code) : Client
    {
        $resultSet = $this->gateway->select(['codcli' => $code]);

        $result = $resultSet->current();

        return $this->buildClient($result);
    }

    public function findAll() : array
    {
        $resultSet = $this->gateway->select();
        $clients = [];
        foreach($resultSet as $row) {
            $client = $this->buildClient($row);
            array_push($clients, $client);
        }

        return $clients;
    }

    public function save(Client $client)
    {
        return $this->gateway->insert([
            'codcli'          => $client->getCode(),
            'nome'            => $client->getName(),
            'cpf'             => $client->getCpf(),
            'cidade'          => $client->getCity(),
            'fisica_juridica' => $client->getTypePerson(),
            'situacao'        => $client->getStatus()
        ]);
    }

    public function update(Client $client)
    {
        $data = [
            'codcli'          => $client->getCode(),
            'nome'            => $client->getName(),
            'cpf'             => $client->getCpf(),
            'cidade'          => $client->getCity(),
            'fisica_juridica' => $client->getTypePerson(),
            'situacao'        => $client->getStatus()
        ];

        $id = $client->getCode();;

        $this->gateway->update($data, ['codcli' => $id]);
    }

    public function delete($id)
    {
        $this->gateway->delete(['codcli' => $id]);
    }

    private function buildClient($data) : Client
    {
        return new Client(
            $data['codcli'],
            $data['nome'],
            $data['cpf'],
            $data['fisica_juridica'],
            $data['cidade'],
            $data['situacao']
        );
    }
}
