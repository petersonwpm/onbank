<?php
namespace Application\Model;

use Application\Entity\Agency;
use Zend\Db\TableGateway\TableGatewayInterface;

class AgencyTable
{
    public function __construct(TableGatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    public function findByCode(string $code) : Agency
    {
        $resultSet = $this->gateway->select(['codag' => $code]);

        $result = $resultSet->current();

        return $this->buildAgency($result);
    }

    public function findAll() : array
    {
        $resultSet = $this->gateway->select();
        $agencies = [];
        foreach ($resultSet as $row) {
            $agencies[] = $this->buildAgency($row);
        }

        return $agencies;
    }

    public function save(Agency $agency)
    {
        return $this->gateway->insert([
            'codag'           => $agency->getCode(),
            'nome'            => $agency->getName(),
            'cidade'          => $agency->getCity(),
        ]);
    }

    public function update(Agency $agency)
    {
        $data = [
            'codag'          => $agency->getCode(),
            'nome'            => $agency->getName(),
            'cidade'          => $agency->getCity(),
        ];

        $id = $agency->getCode();

        $this->gateway->update($data, ['codag' => $id]);
    }

    public function delete($id)
    {
        $this->gateway->delete(['codag' => $id]);
    }

    private function buildAgency($row) : Agency
    {
        return new Agency(
            $row['codag'],
            $row['nome'],
            $row['cidade']
        );
    }

}
