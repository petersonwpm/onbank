<?php
namespace Application\Model;

use Application\Entity\AccountMovementType;
use Zend\Db\TableGateway\TableGatewayInterface;

class AccountMovementTypeTable
{
    /**
     * @var Zend\Db\TableGateway\TableGatewayInterface $gateway
     */
    private $gateway;

    public function __construct(TableGatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }

    public function findByCode(string $code)
    {
        $resultSet = $this->gateway->select(['codmov' => $code]);
        $row = $resultSet->current();

        return $this->buildAccountMovementType($row);
    }

    public function findAll() : array
    {
        $resultSet = $this->gateway->select();
        $movementTypes = [];
        foreach ($resultSet as $row) {
            $movementTypes[] = $this->buildAccountMovementType($row);
        }
        return $movementTypes;
    }

    private function buildAccountMovementType($row)
    {
        return new AccountMovementType(
            $row['codmov'],
            $row['descricao'],
            $row['debito_credito']
        );
    }
}
