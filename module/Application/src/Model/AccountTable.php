<?php
namespace Application\Model;

use Application\Entity\Account;
use Application\Entity\AccountType;
use Application\Entity\Agency;
use Application\Entity\Client;
use Zend\Db\TableGateway\TableGatewayInterface;
use DateTime;

class AccountTable
{
    private $account;
    private $agency;
    private $client;
    private $accountType;

    public function __construct(
        TableGatewayInterface $account,
        TableGatewayInterface $agency,
        TableGatewayInterface $client,
        TableGatewayInterface $accountType
    )
    {
        $this->account     = $account;
        $this->agency      = $agency;
        $this->client      = $client;
        $this->accountType = $accountType;
    }

    public function findByCode(string $code) : Account
    {
        $resultSet = $this->account->select(['conta' => $code]);

        $row = $resultSet->current();

        $tagency = $this->agency->select(['codag' => $row['codag']]);
        $arow = $tagency->current();

        $tclient = $this->client->select(['codcli' => $row['codcli']]);
        $carow = $tclient->current();

        $taccountType = $this->accountType->select(['tipoconta' => $row['tipoconta']]);
        $trow = $taccountType->current();

        $account = $this->buildClient($row);
        $account->setAgency(new Agency($arow['codag'], $arow['nome'], $arow['cidade']));
        $account->setClient(new Client($carow['codcli'], $carow['nome'], $carow['cpf'], $carow['fisica_juridica'], $carow['cidade'], $carow['situacao']));
        $account->setAccountType(new AccountType($trow['tipoconta'], $trow['descricao'], $trow['classe']));

        return $account;
    }

    public function findAll() : array
    {
        $resultSet = $this->account->select();
        $accounts = [];
        foreach($resultSet as $row) {
            $tagency = $this->agency->select(['codag' => $row['codag']]);
            $arow = $tagency->current();

            $tclient = $this->client->select(['codcli' => $row['codcli']]);
            $carow = $tclient->current();

            $taccountType = $this->accountType->select(['tipoconta' => $row['tipoconta']]);
            $trow = $taccountType->current();

            $account = $this->buildClient($row);
            $account->setAgency(new Agency($arow['codag'], $arow['nome'], $arow['cidade']));
            $account->setClient(new Client($carow['codcli'], $carow['nome'], $carow['cpf'], $carow['fisica_juridica'], $carow['cidade'], $carow['situacao']));
            $account->setAccountType(new AccountType($trow['tipoconta'], $trow['descricao'], $trow['classe']));

            $accounts[] = $account;
        }

        return $accounts;
    }

    public function save(Account $account)
    {
        $data = [
            'conta'      => $account->getCode(),
            'codag'      => $account->getAgency()->getCode(),
            'codcli'     => $account->getClient()->getCode(),
            'limit'      => $account->getLimit(),
            'dtabertura' => $account->getOpenDate()->format('Y-m-d'),
            'tipoconta'  => $account->getAccountType()->getCode()
        ];

        $this->account->insert($data);
    }

    public function update(account $account)
    {
        $data = [
            'codag'      => $account->getAgency()->getCode(),
            'codcli'     => $account->getClient()->getCode(),
            'limit'      => $account->getLimit(),
            'dtabertura' => $account->getOpenDate()->format('Y-m-d'),
            'tipoconta'  => $account->getAccountType()->getCode()
        ];

        $this->account->update($data, ['conta' => $account->getCode()]);
    }

    public function delete($id)
    {
        $this->account->delete(['conta' => $id]);
    }

    private function buildClient($data)
    {
        $account = new Account(
            $data['conta'],
            $data['limit']
        );

        return $account;
    }
}
