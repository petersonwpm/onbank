<?php
namespace Application\Model;

use Application\Entity\AccountMovement;
use Application\Model\AccountMovementTypeTable;
use Application\Model\AccountTable;
use DateTime;
use Zend\Db\TableGateway\TableGatewayInterface;

class AccountMovementTable
{
    /**
     * @var Zend\Db\TableGateway\TableGatewayInterface $gateway
     */
    private $gateway;

    /**
     * @var Application\Model\AccountTable $account
     */
    private $account;

    /**
     * @var Application\Model\AccountMovementTypeTable $movementType
     */
    private $movementType;

    public function __construct(
        TableGatewayInterface $gateway,
        AccountTable $account,
        AccountMovementTypeTable $movementType
    )
    {
        $this->gateway      = $gateway;
        $this->account      = $account;
        $this->movementType = $movementType;
    }

    public function save(AccountMovement $movement)
    {
        $data = [
            'conta'  => $movement->getAccount()->getCode(),
            'codmov' => $movement->getType()->getCode(),
            'data'   => $movement->getDate()->format('Y-m-d'),
            'valor'  => $movement->getValue()
        ];

        $this->gateway->insert($data);
    }

    public function findAll() : array
    {
        $resultSet = $this->gateway->select();
        $movements = [];
        foreach ($resultSet as $row) {
            $movements[] = $this->buildMovement($row);
        }

        return $movements;
    }

    public function findByAccountId(string $id) : array
    {
        $resultSet = $this->gateway->select(['conta' => $id]);
        $movements = [];
        foreach ($resultSet as $row) {
            $movements[] = $this->buildMovement($row);
        }

        return $movements;
    }

    private function buildMovement($row) : AccountMovement
    {
        $account      = $this->account->findByCode($row['conta']);
        $movementType = $this->movementType->findByCode($row['codmov']);

        return new AccountMovement(
            $account,
            $movementType,
            new DateTime($row['data']),
            $row['valor']
        );
    }

}
