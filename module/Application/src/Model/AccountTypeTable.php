<?php
namespace Application\Model;

use Application\Entity\AccountType;
use Zend\Db\TableGateway\TableGatewayInterface;

class AccountTypeTable
{
    /**
     * @var TableGatewayInterface $gateway
     */
    private $gateway;

    public function __construct(TableGatewayInterface $accountType)
    {
        $this->gateway = $accountType;
    }

    public function findAll()
    {
        $resultSet = $this->gateway->select();
        $types = [];
        foreach ($resultSet as $row) {
            $types[] = $this->buildAccountType($row);
        }

        return $types;
    }

    public function findByCode(string $code) : AccountType
    {
        $resultSet = $this->gateway->select();
        $row = $resultSet->current();

        return $this->buildAccountType($row);
    }

    private function buildAccountType($row)
    {
        return new AccountType(
            $row['tipoconta'],
            $row['descricao'],
            $row['classe']
        );
    }
}
