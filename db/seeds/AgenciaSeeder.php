<?php


use Phinx\Seed\AbstractSeed;

class AgenciaSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'codag' => 'A1',
                'nome'  => 'Central',
                'cidade' => 'Porto Alegre'
            ],
            [
                'codag' => 'A2',
                'nome'  => 'Moinhos de Ventos',
                'cidade' => 'Porto Alegre'
            ],
            [
                'codag' => 'A3',
                'nome'  => 'Praça Central',
                'cidade' => 'Caxias do Sul'
            ],
            [
                'codag' => 'A4',
                'nome'  => 'Muck',
                'cidade' => 'Canoas'
            ],
        ];

        $this->table('agencias')
             ->insert($data)
             ->save();
    }
}
