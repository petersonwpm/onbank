<?php


use Phinx\Seed\AbstractSeed;

class ContasSeeder extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'TipoDeContaSeeder',
            'ClienteSeeder',
            'AgenciaSeeder',
        ];
    }

    public function run()
    {
        $data = [
            [
                'conta' => '0001',
                'codag' => 'A1',
                'codcli' => 'C1',
                'limit' => 0,
                'dtabertura' => '1995/01/01',
                'tipoconta'  => 'T1',
            ],
            [
                'conta' => '0002',
                'codag' => 'A1',
                'codcli' => 'C2',
                'limit' => 1250.00,
                'dtabertura' => '1995/03/01',
                'tipoconta'  => 'T4',
            ],
            [
                'conta' => '0003',
                'codag' => 'A1',
                'codcli' => 'C1',
                'limit' => 0,
                'dtabertura' => '1995/03/01',
                'tipoconta'  => 'T2',
            ],
            [
                'conta' => '0004',
                'codag' => 'A2',
                'codcli' => 'C3',
                'limit' => 0,
                'dtabertura' => '1998/10/01',
                'tipoconta'  => 'T2',
            ],
            [
                'conta' => '0005',
                'codag' => 'A3',
                'codcli' => 'C1',
                'limit' => 2000.00,
                'dtabertura' => '1999/02/01',
                'tipoconta'  => 'T1',
            ],
        ];

        $this->table('contas')
             ->insert($data)
             ->save();
    }
}
