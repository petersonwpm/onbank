<?php


use Phinx\Seed\AbstractSeed;

class TiposDeMovimentoSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'codmov' => 'M1',
                'descricao' => 'Retirada em Dinheiro',
                'debito_credito' => 'D',
            ],
            [
                'codmov' => 'M2',
                'descricao' => 'Depósito em Dinheiro',
                'debito_credito' => 'C',
            ],
            [
                'codmov' => 'M3',
                'descricao' => 'Tarifa',
                'debito_credito' => 'D',
            ],
            [
                'codmov' => 'M4',
                'descricao' => 'Crédito de Juros',
                'debito_credito' => 'C',
            ],
        ];

        $this->table('tipos_de_movimento')
             ->insert($data)
             ->save();
    }
}
