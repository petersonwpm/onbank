<?php


use Phinx\Seed\AbstractSeed;

class MovimentosSeeder extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'ContasSeeder',
            'TiposDeMovimentoSeeder',
        ];
    }

    public function run()
    {
        $data = [
            [
                'conta' => '0001',
                'codmov' => 'M1',
                'data' => '1999/01/02',
                'valor' => 100.00,
            ],
            [
                'conta' => '0002',
                'codmov' => 'M2',
                'data' => '1999/02/01',
                'valor' => 50.00,
            ],            [
                'conta' => '0003',
                'codmov' => 'M1',
                'data' => '1999/05/11',
                'valor' => 535.00,
            ],            [
                'conta' => '0001',
                'codmov' => 'M1',
                'data' => '1999/05/11',
                'valor' => 250.00,
            ],
        ];

        $this->table('movimentos')
             ->insert($data)
             ->save();
    }
}
