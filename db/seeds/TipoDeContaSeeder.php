<?php


use Phinx\Seed\AbstractSeed;

class TipoDeContaSeeder extends AbstractSeed
{

    public function run()
    {
        $data = [
            [
                'tipoconta' => 'T1',
                'descricao' => 'Conta Corrente Especial',
                'classe'    => 'C',
            ],
            [
                'tipoconta' => 'T2',
                'descricao' => 'Conta Corrente',
                'classe'    => 'C',
            ],
            [
                'tipoconta' => 'T3',
                'descricao' => 'Poupança',
                'classe'    => 'P',
            ],
            [
                'tipoconta' => 'T4',
                'descricao' => 'CDB',
                'classe'    => 'A',
            ],
        ];

        $this->table('tipos_de_conta')
             ->insert($data)
             ->save();
    }
}
