<?php


use Phinx\Seed\AbstractSeed;

class ClienteSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'codcli' => 'C1',
                'nome'   => 'Jose da Silva',
                'cpf'    => '11145678990',
                'fisica_juridica' => 'F',
                'cidade' => 'Porto Alegre',
            ],[
                'codcli' => 'C2',
                'nome'   => 'Joao Menezes',
                'cpf'    => '22245678970',
                'fisica_juridica' => 'F',
                'cidade' => 'Porto Alegre',
            ],[
                'codcli' => 'C3',
                'nome'   => 'Transportadora Tresmaiense',
                'cpf'    => '33345678905',
                'fisica_juridica' => 'J',
                'cidade' => 'Porto Alegre',
            ],[
                'codcli' => 'C4',
                'nome'   => 'Lojas Rigatoni',
                'cpf'    => '44445678945',
                'fisica_juridica' => 'J',
                'cidade' => 'São Paulo',
            ],[
                'codcli' => 'C5',
                'nome'   => 'Maria do Rosário',
                'cpf'    => '55545678923',
                'fisica_juridica' => 'F',
                'cidade' => 'Porto Alegre',
            ],
        ];

        $this->table('clientes')->insert($data)->save();
    }
}
