<?php


use Phinx\Migration\AbstractMigration;

class CreateTableAgencias extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('agencias', ['id' => false, 'primary_key' => 'codag']);
        $table
            ->addColumn('codag', 'string')
            ->addColumn('nome', 'string')
            ->addColumn('cidade', 'string');

        $table->create();
    }
}
