<?php


use Phinx\Migration\AbstractMigration;

class CreateTableClientes extends AbstractMigration
{
    public function change()
    {
        $this
            ->table('clientes', ['id' => false, 'primary_key' => 'codcli'])
            ->addColumn('codcli', 'string')
            ->addColumn('nome', 'string')
            ->addColumn('cpf', 'string')
            ->addColumn('fisica_juridica', 'string', ['limit' => 1])
            ->addColumn('cidade', 'string')
            ->create();
    }
}
