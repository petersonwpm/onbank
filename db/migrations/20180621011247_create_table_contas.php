<?php


use Phinx\Migration\AbstractMigration;

class CreateTableContas extends AbstractMigration
{
    public function change()
    {
        $this->table('contas', ['id' => false, 'primary_key' => 'conta'])
             ->addColumn('conta', 'string')
             ->addColumn('codag', 'string')
             ->addColumn('codcli', 'string')
             ->addColumn('limit', 'decimal')
             ->addColumn('dtabertura', 'date')
             ->addColumn('tipoconta', 'string')
             ->addForeignKey('codag', 'agencias', 'codag')
             ->addForeignKey('codcli', 'clientes', 'codcli')
             ->addForeignKey('tipoconta', 'tipos_de_conta', 'tipoconta')
             ->create();
    }
}
