<?php


use Phinx\Migration\AbstractMigration;

class CreateTableTiposDeMovimento extends AbstractMigration
{
    public function change()
    {
        $this->table('tipos_de_movimento', ['id' => false, 'primary_key' => 'codmov'])
             ->addColumn('codmov', 'string')
             ->addColumn('descricao', 'string')
             ->addColumn('debito_credito', 'string')
             ->create();
    }
}
