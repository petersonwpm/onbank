<?php


use Phinx\Migration\AbstractMigration;

class AlterTableAddSituacaoClientes extends AbstractMigration
{
    public function change()
    {
        $this->table('clientes')
             ->addColumn('situacao', 'boolean', ['default' => true])
             ->update();
    }
}
