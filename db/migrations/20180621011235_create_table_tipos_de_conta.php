<?php


use Phinx\Migration\AbstractMigration;

class CreateTableTiposDeConta extends AbstractMigration
{
    public function change()
    {
        $this->table('tipos_de_conta', ['id' => false, 'primary_key' => 'tipoconta'])
             ->addColumn('tipoconta', 'string')
             ->addColumn('descricao', 'string')
             ->addColumn('classe', 'string')
             ->create();
    }
}
