<?php


use Phinx\Migration\AbstractMigration;

class CreateTableMovimentos extends AbstractMigration
{
    public function change()
    {
        $this->table('movimentos', ['id' => false, 'primary_key' => ['conta', 'codmov', 'data']])
             ->addColumn('conta', 'string')
             ->addColumn('codmov', 'string')
             ->addColumn('data', 'date')
             ->addColumn('valor', 'decimal')
             ->addForeignKey('conta', 'contas', 'conta')
             ->addForeignKey('codmov', 'tipos_de_movimento', 'codmov')
             ->create();
    }
}
